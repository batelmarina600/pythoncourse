def are_lists_equal(list1, list2):
    """ The function returns true if both lists contain exactly the same elements 
    (even if in different order), otherwise, the function returns false
    :param list_x: the first list 
    :param list_y: the second list
    :type list_x: list
    :type list_y: list
    :return: if the items in both lists are the same-return Ture, else False
    :rtype: bool
    """
    list1.sort()
    list2.sort()
    if(list1 == list2):
        return True
    else:
        return False
    
list1 = [0.6, 1, 2, 3]
list2 = [3, 2, 0.6, 1]
list3 = [9, 0, 5, 10.5]
print(are_lists_equal(list1, list2))
print(are_lists_equal(list1, list3))

