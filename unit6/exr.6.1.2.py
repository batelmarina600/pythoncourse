def shift_left(my_list):
    """ shifts left every item in the list, the first one goes to the end
    :param my_list: the list that going to be shifted
    :type my_list: list
    :return: the shifted list
    :rtype: list
    """
    my_list[0],my_list[1],my_list[2] = my_list[1],my_list[2],my_list[0]
    return my_list

print(shift_left([0,1,2]))
print(shift_left(['monkey', 2.0, 1]))

