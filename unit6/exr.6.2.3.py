def format_list(my_list):
    """ format_list on the given orders, takes the even placed items, puts "," and end with "and" 
    and the last item.
    :param my_list: the list that going to be fomated
    :type my_list: list
    :return: the formated list
    :rtype: list
    """
    new_list = ", ".join(my_list[0::2])
    return new_list + " and " + my_list[-1]

my_list = ["hydrogen", "helium", "lithium", "beryllium", "boron", "magnesium"]
print(format_list(my_list))

