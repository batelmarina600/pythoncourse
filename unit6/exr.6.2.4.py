def extend_list_x(list_x, list_y):
    """ The function expands list_x (changes it)
    so that it also contains list_y at the beginning, and returns list_x.
    :param list_x: the first list 
    :param list_y: the second list that going to be added to the beginning of list_x
    :type list_x: list
    :type list_y: list
    :return: the extend list_x that is list_y + list_x
    :rtype: list
    """
    list_x = [*list_y, *list_x]
    return list_x

x = [4, 5, 6]
y = [1, 2, 3]
print(extend_list_x(x, y))