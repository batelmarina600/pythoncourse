def longest(my_list):
    """ The function receives a list of strings and returns the longest string
    :param my_list: the list that the function going to search on
    :type my_list: list
    :return: the longest word in my_list
    :rtype: string
    """
    my_list = sorted(my_list, key = len)
    return my_list[-1]

list1 = ["111", "234", "2000", "goru", "birthday", "09"]
print(longest(list1))