def try_update_letter_guessed(letter_guessed, old_letters_guessed):
    """ The function returns True if its a valid character.
    if the string is one english letter that is not in old_letters_guessed
    and if yes- it adds it to the old_letters_guessed. if not prints X and the letters 
    in old_letters_guessed sorted.

    :param letter_guessed: the guessed letter that the function going to check
    :param old_letters_guessed: list of the guessed letters
    :type letter_guessed: list
    :type old_letters_guessed: list
    :return: True if the letter_guessed is one english letter and not in old_letters_guessed else False
    :rtype: bool
    """
    letter = letter_guessed.lower()
    if ((len(letter) == 1) and (letter.isalpha()) and (not letter in old_letters_guessed)):
        old_letters_guessed.append(letter_guessed)
        return True
    else:
        print("X")   
        new_list = " -> ".join(sorted(old_letters_guessed))
        print(new_list)
        return False
    

old_letters = ['a', 'p', 'c', 'f']
#print(try_update_letter_guessed('A', old_letters))
#print(try_update_letter_guessed('s', old_letters))
#print(try_update_letter_guessed('$', old_letters))
print(try_update_letter_guessed('d', old_letters))
print(old_letters)