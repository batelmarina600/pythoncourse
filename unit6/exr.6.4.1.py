def check_valid_input(letter_guessed, old_letters_guessed):
    """ The function returns True if its a valid character.
    if the string is one english letter that is not in old_letters_guessed
    :param letter_guessed: the guessed letter that the function going to check
    :param old_letters_guessed: list of the guessed letters
    :type letter_guessed: list
    :type old_letters_guessed: list
    :return: True if the letter_guessed is one english letter and not in old_letters_guessed else False
    :rtype: bool
    """
    letter = letter_guessed.lower()
    if ((len(letter) == 1) and (letter.isalpha()) and (not letter in old_letters_guessed)):
        return True
    else:
        return False
    
old_letters = ['a', 'b', 'c']
print(check_valid_input('C', old_letters))
print(check_valid_input('ep', old_letters))
print(check_valid_input('$', old_letters))
print(check_valid_input('s', old_letters))

