

def check_win(secret_word, old_letters_guessed):
    """The function returns true if all the letters that make up the secret word are included 
    in the list of letters that the user guessed. Otherwise, the function returns false.
    :param secret_word: represents the secret word that the player has to guess
    :param old_letters_guessed: a list contains the letters the player has guessed so far
    :type secret_word: string
    :type old_letters_guessed: list
    :return: True if there is a win, False if not
    :rtype: bool
    """
    for char in secret_word:
        if(not (char in old_letters_guessed)): #if there is an letter in secret_word that wasn't guessed
            return False
    return True

######################################################

def show_hidden_word(secret_word, old_letters_guessed):
    """The string shows the letters from the old_letters_guessed list that are in the secret_word string 
    in their appropriate position, and the other letters in the string 
    (which the player has not yet guessed) as underlines.
    :param secret_word: represents the secret word that the player has to guess
    :param old_letters_guessed: a list contains the letters the player has guessed so far
    :type secret_word: string
    :type old_letters_guessed: list
    :return: the string that can be printed 
    :rtype: string
    """
    print_str = ""
    for char in secret_word:
        if(char in old_letters_guessed): 
            print_str += char 
        else:
            print_str += "_"
        print_str += " "

    return print_str

##############################################################

def check_valid_input(letter_guessed, old_letters_guessed):
    """ The function returns True if its a valid character.
    if the string is one english letter that is not in old_letters_guessed
    :param letter_guessed: the guessed letter that the function going to check
    :param old_letters_guessed: list of the guessed letters
    :type letter_guessed: list
    :type old_letters_guessed: list
    :return: True if the letter_guessed is one english letter and not in old_letters_guessed else False
    :rtype: bool
    """   
    letter = letter_guessed.lower()
    return ((len(letter) == 1) and (letter.isalpha()) and (not letter in old_letters_guessed))

#######################################################

def try_update_letter_guessed(letter_guessed, old_letters_guessed):
    """ The function returns True if its a valid character.
    if the string is one english letter that is not in old_letters_guessed
    and if yes- it adds it to the old_letters_guessed. if not prints X and the letters 
    in old_letters_guessed sorted.

    :param letter_guessed: the guessed letter that the function going to check
    :param old_letters_guessed: list of the guessed letters
    :type letter_guessed: list
    :type old_letters_guessed: list
    :return: True if the letter_guessed is one english letter and not in old_letters_guessed else False
    :rtype: bool
    """
    if (check_valid_input(letter_guessed, old_letters_guessed)):
        old_letters_guessed.append(letter_guessed) #if valid-add
        return True
    else:
        print("X")   
        new_list = " -> ".join(sorted(old_letters_guessed))
        print(new_list)
        return False
       
#############################################################

def choose_word(file_path, index):
    """The function takes in a file path and an index and returns a word from a file 
    according to the given index
    :param file_path: file path
    :param index: int numer representing index
    :type file_path: string
    :type index: int 
    :return: tuple contains amount of unique words and the chosen words
    :rtype: string
    """
    file = open(file_path, "r")
    data = file.read()
    words_list = data.split(" ")

    string = words_list[index % len(words_list) - 1] #in the index place
    return string

###############################################################

HANGMAN_PHOTOS = {0: "x-------x", 1:
"""    x-------x 
    |
    |
    |
    |
    |""",
2:
"""    x-------x
    |       |
    |       0
    |
    |
    |""",
3:
"""    x-------x
    |       |
    |       0
    |       |
    |
    |""",
4:
"""    x-------x
    |       |
    |       0
    |      /|\\
    |
    |""",

5:
"""    x-------x
    |       |
    |       0
    |      /|\\
    |      /
    |""",

6:
"""    x-------x
    |       |
    |       0
    |      /|\\
    |      / \\
    |"""}


def print_hangman(num_of_tries):
    """ The function prints one of the seven states of the hanging man, using:
    the num_of_tries of the user and The HANGMAN_PHOTOS variable
    :param num_of_tries: a number of failed attempts by the user so far.
    :type num_of_tries: int
    :return: none
    :rtype: none
    """ 
    print(HANGMAN_PHOTOS[num_of_tries])

def opening_screen():
    """ for the opening screen of the game
    :param : none
    :type : none
    :return: none
    :rtype: none
    """ 
    print("Welcome to the game Hangman\n")
    HANGMAN_ASCII_ART =  """  _    _                                         
 | |  | |                                        
 | |__| | __ _ _ __   __ _ _ __ ___   __ _ _ __  
 |  __  |/ _` | '_ \ / _` | '_ ` _ \ / _` | '_ \ 
 | |  | | (_| | | | | (_| | | | | | | (_| | | | |
 |_|  |_|\__,_|_| |_|\__, |_| |_| |_|\__,_|_| |_|
                      __/ |                      
                     |___/"""
    print(HANGMAN_ASCII_ART)
    print(MAX_TRIES)


###########################################################################

MAX_TRIES = 6
    
def main():
    
    opening_screen() 
    
    path = input("Please enter a path for the words file: ")
    index = input("Please enter an index of a word in the file: ")
    
    secret_word = choose_word(path, int(index))
    num_of_tries = 0
    old_letters_guessed = []
    flag = False
    print("LETS STRAT!")

    while num_of_tries < MAX_TRIES :
       
        print(show_hidden_word(secret_word, old_letters_guessed))
        if check_win(secret_word, old_letters_guessed):
            flag = True
            print("WIN!")
            break
        
        while True:
            guess_letter = input("Guess a letter: ")
            valid = try_update_letter_guessed(guess_letter, old_letters_guessed) #checks if valid
            if not valid:
                print("X")
                continue #to try again
            elif guess_letter.lower() not in secret_word.lower():
                num_of_tries +=1
                print("      ): ")
                print_hangman(num_of_tries)
                break
            else:
                break
    if num_of_tries == MAX_TRIES and not flag: #got to MAX_TRIES and didn't win
        print("LOSE!")
            

if __name__=="__main__":
    main()