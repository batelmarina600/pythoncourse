char_guess = input("Guess a letter:")

if (len(char_guess) > 1) and (all(char.isalpha() for char in char_guess)):
    print("E1")
elif ((len(char_guess) == 1) and (not char_guess.isalpha())):
    print("E2")
elif (len(char_guess) > 1) and (not all(char.isalpha() for char in char_guess)):
    print("E3")
else:
    print(char_guess.lower())

