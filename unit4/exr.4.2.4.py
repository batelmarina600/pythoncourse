import datetime
import calendar

date_str = input("Enter a date in dd/mm/yyyy format: ")
date = datetime.datetime.strptime(date_str, '%d/%m/%Y') #convert string to datetime

week_day = date.weekday() #gets the weekday

week_day_str = calendar.day_name[week_day] #name of day
print(week_day_str)
