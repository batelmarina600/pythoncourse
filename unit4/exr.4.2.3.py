degrees = input("Insert the temperature you would like to convert:")
type_degree = degrees[-1]
num_degrees = float(degrees[:-1])

if (type_degree.lower() == "c"):
    res = (9 * num_degrees + (32 * 5)) / 5
    print(str(res) + "F")
elif (type_degree.lower() == "f"):
    res = (5 * num_degrees - 160) / 9
    print(str(res) + "C")

