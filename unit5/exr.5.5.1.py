def is_valid_input(letter_guessed):
    """ Checks if the guessed letter is valid - is one char and in english
    :param letter_guessed: guessed letter
    :type letter_guessed: string
    :return: True if valid, False if not
    :rtype: Bool
    """
    if ((len(letter_guessed) == 1) and (letter_guessed.isalpha())):
        return True
    else:
        return False

print(is_valid_input('app$'))