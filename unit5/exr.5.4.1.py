def func(num1, num2):
    """ Calculates the multiplication of two numbers.
    :param num1: first number
    :param num2: second number
    :type num1: int
    :type num2: int
    :return: the result of num1 multiply num2
    :rtype: int
    """
    return num1 * num2

def main():
    help(func) #shows the documention of the function func
    print(func(3, 4)) #print the result of 3 * 4

if __name__ == "__main__":
    main()
