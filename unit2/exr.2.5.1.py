string = "Welcome to the game Hangman\n"
HANGMAN_ASCII_ART =  """   _    _
   | |  | |
   | |__| | __ _ _ __   __ _ _ __ ___   __ _ _ __
   |  __  |/ _' | '_ \ / _' | '_ ' _ \ / _' | '_ \\
   | |  | | (_| | | | | (_| | | | | | | (_| | | | |
   |_|  |_|\__,_|_| |_|\__, |_| |_| |_|\__,_|_| |_|
                        __/ |
                       |___/\n"""

MAX_TRIES = 6

print(string , HANGMAN_ASCII_ART, MAX_TRIES)