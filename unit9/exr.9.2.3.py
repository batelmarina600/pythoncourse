def who_is_missing(file_name):
    """ The function needs to find the missing number in a sequence of integers read from a file, 
    and then write that missing number to a new file called "found.txt".
    :param file_name: the file path
    :type file_name: string
    :return: none
    :rtype: none
    """ 
    with open(file_name, 'r') as file:
            numbers = file.read()
    numbers = numbers.replace(",", "")
    max_number = max(numbers)
    
    for num in range(1, int(max_number) + 1):
        if str(num) not in numbers:
            missing_num = num
            break
    with open("C:\\Users\\User\\found.txt", 'w') as res_file:
        res_file.write(str(missing_num))
              
who_is_missing("C:\\Users\\User\\file01.txt")
