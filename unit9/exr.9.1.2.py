path = input("Enter a file path: ")
task = input("Enter a task: ")

if task == "sort":
    with open(path, 'r') as file:
        words = set(file.read().split())
        print(sorted(words))
    
if task == "rev":
    with open(path, 'r') as file:
        lines = file.readlines()
        for line in lines:
            print(line.rstrip()[::-1])
        
if task == "last":
    n = int(input("Enter a number: "))
    with open(path, 'r') as file:
        lines = file.readlines()
        for line in lines[-n:]:
            print(line.rstrip())
       