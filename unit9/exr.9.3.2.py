def my_mp4_playlist(file_path, new_song):
    """The function accepts path to a file(file represents a playlist of songs) and a new_string
    and it writes to the file the string representing the name of a new song (new_song) 
    instead of the name of the song appearing in the third line of the file
    The function prints the contents of the file after the change has been made.
    :param file_path: file path
    :param new_song: the new song
    :type file_path: string
    :type new_song: string 
    :return: none
    :rtype: none
    """
    with open(file_path, 'r') as file:
        lines = file.readlines()

    if len(lines) < 3:
        lines += ["\n"] * (3 - len(lines)) 
 
    song_line = lines[2].strip().split(";")
    song_line[0] = new_song
    lines[2] = ";".join(song_line) + "\n"

    file1 = open(file_path, 'w')
    file1.writelines(lines)

    file2 = open(file_path, 'r')
    print(file2.read())
    

my_mp4_playlist("songs.txt", "Python Love Story")