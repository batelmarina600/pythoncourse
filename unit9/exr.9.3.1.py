def my_mp3_playlist(file_path):
    """The function reads a file containing a playlist of songs and returns a tuple that includes 
    the name of the longest song in the playlist, the number of songs in the playlist, 
    and the most popular srtist 
    :param file_path: the file path
    :type file_path: string
    :return: a tuple with the longest song, amount og songs and the most popular artist
    :rtype: tuple
    """ 
    songs = []
    artists = {}
    with open(file_path, 'r') as f:
        for line in f:
            song, artist, length = line.strip().split(';')
            songs.append((song, artist, length)) 
            artists[artist] = artists.get(artist, 0) + 1
    
    longest_song = max(songs, key=lambda x: x[2])[0]
    num_songs = len(songs)
    popular_artist = max(artists, key=artists.get)
    
    return (longest_song, num_songs, popular_artist)

print(my_mp3_playlist(r"C:\songs.txt"))