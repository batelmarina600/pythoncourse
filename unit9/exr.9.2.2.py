def copy_file_content(source, destination):
    """The function accepts as parameters two strings representing file paths.
    The function copies the content of the source file to the destination file.
    :param source: first file path
    :param destination: second file path
    :type source: string
    :type destination: string 
    :return: none
    :rtype: none
    """
    source_file = open(source,"r")
    source_text = source_file.read()
    source_file.close()

    dest_file = open(destination,"w")
    dest_file.write(source_text)
    dest_file.close()
   
copy_file_content("copy.txt", "paste.txt")


