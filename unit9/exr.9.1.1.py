def are_files_equal(file1, file2):
    """The function accepts as parameters paths of two text files (strings) 
    and checks if their content is the same.
    :param file1: first file
    :param file2: second file
    :type file1: file
    :type file2: file 
    :return: True if the files are identical in content, otherwise returns False.
    :rtype: bool
    """
    with open(file1,"r") as file01, open(file2,"r") as file02:
        text1 = file01.read()
        text2 = file02.read()
        return text1 == text2

print(are_files_equal("C:\Users\User\file01.txt", "C:\Users\User\file02.txt"))