def choose_word(file_path, index):
    """The function takes in a file path and an index and returns a tuple containing the number of 
    unique words in the file and the word at the specified index. words in a text corpus.
    and it selects a random word from a file according to the given index
    :param file_path: file path
    :param index: int numer representing index
    :type file_path: string
    :type index: int 
    :return: tuple contains amount of unique words and the chosen words
    :rtype: tuple
    """
    file = open(file_path, "r")
    data = file.read()
    words_list = data.split(" ")
    uniques = list(dict.fromkeys(words_list))

    res = []
    res.append(len(uniques))
    res.append(words_list[index % len(words_list) - 1]) 
    return tuple(res)

print(choose_word(r"c:\words.txt", 3))