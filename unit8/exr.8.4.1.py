HANGMAN_PHOTOS = {1: "x-------x", 2:
""" x-------x 
    |
    |
    |
    |
    |""",
3:
"""    x-------x
    |       |
    |       0
    |
    |
    |""",
4:
"""    x-------x
    |       |
    |       0
    |       |
    |
    |""",
5:
"""    x-------x
    |       |
    |       0
    |      /|\\
    |
    |""",

6:
"""    x-------x
    |       |
    |       0
    |      /|\\
    |      /
    |""",

7:
"""    x-------x
    |       |
    |       0
    |      /|\\
    |      / \\
    |"""}



def print_hangman(num_of_tries):
    """ The function prints one of the seven states of the hanging man, using:
    the num_of_tries of the user and The HANGMAN_PHOTOS variable
    :param num_of_tries: a number of failed attempts by the user so far.
    :type num_of_tries: int
    :return: none
    :rtype: none
    """ 
    print(HANGMAN_PHOTOS[num_of_tries])

num_of_tries = 6
print_hangman(num_of_tries)