def mult_tuple(tuple1, tuple2):
    """The function accepts two tuples.
    The function returns a tuple containing all the pairs that can be created from the members
    of the tuples passed as arguments.
    :param tuple1: first tuple
    :param tuple2: second tuple
    :type tuple1: tuple
    :type tuple2: tuple 
    :return: a tuple of tupls containing all the possible pairs
    :rtype: a tuple of tuples
    """
    new_list = []
    for i in range(len(tuple1)):
        for j in range(len(tuple2)):
            new_list.append((tuple1[i], tuple2[j]))
            new_list.append((tuple2[j], tuple1[i]))
    return tuple(new_list)


first_tuple = (1, 2)
second_tuple = (4, 5)
print(mult_tuple(first_tuple, second_tuple))

#first_tuple = (1, 2, 3)
#second_tuple = (4, 5, 6)
#print(mult_tuple(first_tuple, second_tuple))