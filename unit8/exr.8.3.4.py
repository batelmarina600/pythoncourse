def inverse_dict(my_dict):
    """ The function accepts a dictionary. The function returns a new dictionary with a "reverse" mapping:
    each key in the passed dictionary is a value in the returned dictionary and each value in the passed 
    dictionary is a key in the returned dictionary.
    if in the new dictionary there is going to be more than one value for the key- the values 
    going to be saved in a list. and those list will be sorted.
    :param my_dict: the dictionary that going to be searched on
    :type my_dict: dictionary
    :return: the reverse mapping dictionary, (switching the values and keys)
    :rtype: dictionary
    """ 
    new_dict = {}
    for key, value in my_dict.items():
        if value in new_dict:
            new_dict[value].append(key)   
        else:
            new_dict[value] = [key]    
    for value in new_dict.values():
        value.sort()
    return new_dict


course_dict = {'I': 3, 'love': 3, 'self.py!': 2}
print(inverse_dict(course_dict))