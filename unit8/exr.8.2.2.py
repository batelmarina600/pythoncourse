def sort_prices(list_of_tuples): 
    """ The function  The function receives a list of tuples that each have an item and a price.
    The function returns a list of tuples sorted by the price of the items in them from the largest to the smallest.
    :param list_of_tuples: the list that the function going to search on
    :type list_of_tuples: list of tuple, each contains two string first of item and second for a float number
    :return: a sorted tuple by function get_price, in reverse 
    :rtype: list of typle
    """ 
    return sorted(list_of_tuples, key=get_price, reverse=True)

def get_price(tuple1):
    """ The function  The function receives a tuple and return the float value of the tuple[1]
    :param tuple1: the tuple that the function going to work on
    :type tuple1: tuple that contains two string, first of item and second for a float number
    :return: the float value of the second value in the tuple
    :rtype: tuple
    """ 
    return float(tuple1[1])


products = [('milk', '5.5'), ('candy', '2.5'), ('bread', '9.0')]
print(sort_prices(products))