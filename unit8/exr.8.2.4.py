def sort_anagrams(list_of_strings):
    """ The function accepts list of strings.
    The function returns a list of the same strings that were transferred, but in the following way: 
    the list is divided into lists so that each "internal" list consists of words that are anagrams 
    of each other (anagram: a word consisting of letters of another word, i.e. the same letters but 
    in a different order).
    :param list_of_strings: string so each string is one word (without spaces).
    :type list_of_strings: list of strings
    :return: a list of all the list that each contains the anagram words
    :rtype: list (list of list of strings)
    """ 
    res = {}
    for string in list_of_strings:
        sorted_string = "".join(sorted(string))
        if sorted_string in res:
            res[sorted_string].append(string)
        else:
            res[sorted_string] = [string]
    return list(res.values())


list_of_words = ['deltas', 'retainers', 'desalt', 'pants', 'slated', 'generating', 'ternaries', 'smelters', 'termless', 'salted', 'staled', 'greatening', 'lasted', 'resmelts']
print(sort_anagrams(list_of_words))

