def count_chars(my_str):
    """ The function accepts a string. The function returns a dictionary, so that each element in it is
    a pair consisting of a key: a character from the passed string (not including spaces), 
    and a value: the number of times the character appears in the string.
    :param my_str: the string that going to be searched on
    :type my_str: a string
    :return: dictionary that kers are letters and values are their frequency
    :rtype: dictionary
    """ 
    result = {}
    for char in my_str:
        if not char == " ":
            if char in result:
                result[char] += 1
            else:
                result[char] = 1
    return result

magic_str = "abra cadabra"
print(count_chars(magic_str))