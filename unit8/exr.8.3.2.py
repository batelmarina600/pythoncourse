my_dict = {"first_name": "Batel", "last_name":"Liberman" , "birth_date": "06.06.2004", "hobbies": ["Playing sports", "Music"] }

input_num = input("Enter a number between 1 and 7: ")
input_num = int(input_num)
if(input_num == 1):
    print(my_dict["last_name"])
if(input_num == 2):
    print(my_dict["birth_date"][3:5])
if(input_num == 3):
    print(len(my_dict["hobbies"]))
if(input_num == 4):
    print(my_dict["hobbies"][-1])
if(input_num == 5):
    my_dict["hobbies"].append("Cooking")
    print(my_dict["hobbies"])
if(input_num == 6):
    string = my_dict["birth_date"]
    tuple = (string[:2],string[3:5], string[6:])
    my_dict["birth_date"] = tuple
    print(tuple)
if(input_num == 7):
    my_dict["age"] = 53
    print(my_dict["age"])



