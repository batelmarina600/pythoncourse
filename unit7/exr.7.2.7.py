def arrow(my_char, max_length):
    """The function accepts two parameters: the first is a single character, the second is a maximum size.
    The function returns a string representing an "arrow" structure 
    :param my_char: a char that going to be printed
    :param max_length: max of my_char in a row
    :type my_char: char
    :type max_length: int
    :return: nothing
    :rtype: none
    """
    for i in range(1, max_length + 1):
        print(my_char * i)
    for i in range(max_length - 1, 0, -1):
        print(my_char * i)

print(arrow('*', 5))