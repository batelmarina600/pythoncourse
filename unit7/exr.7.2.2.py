def numbers_letters_count(my_str):
    """ The function accepts as a string parameter.
    The function returns a list in which the first element represents the number of digits in the string, 
    and the second element represents the number of letters in the string, including spaces, periods, 
    punctuation marks and everything that is not a digit
    :param my_str: the list that the function going to search on
    :type my_str: list
    :return: list that the list[0] is how many digits were in the string, 
    and list[1] how many chars that are not digits 
    :rtype: list
    """
    digits = 0
    letters = 0
    for letter in my_str:
        if(letter.isdigit()):
            digits += 1
        else:
            letters += 1
    return [digits, letters]

print(numbers_letters_count("Python 3.6.3"))