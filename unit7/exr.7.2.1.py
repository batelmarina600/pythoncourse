def is_greater(my_list, n):
    """The function returns a new list containing all the elements that are greater than the number n.
    :param my_list: list that going to searched on
    :param n: a number, checking the numbers in the list will be relative to this number
    :type my_list: list
    :type n: int
    :return: list containing all the items in my_list who are bigger than n
    :rtype: list
    """
    new_list = []
    for num in my_list:
        if(num > n):
            new_list.append(num)
    return new_list

result = is_greater([1, 30, 25, 60, 27, 28], 28)
print(result)