def seven_boom(end_number):
    """The function accepts an integer, end_number.
    The function returns a list in the range of numbers 0 to end_number inclusive,
    with certain numbers replaced by the string 'BOOM', if they are a multiple of the number 7.
    or containing the digit 7.
    :param end_number: the last number for the sequence
    :type end_number: int
    :return: a list of numbers from 0 to end_numbers, 
    when numbers that are multiple of 7 or have 7- in the list there will be 'BOOM'
    :rtype: list
    """
    new_list = []
    for number in range(end_number+1):
        if(number % 7 == 0):
            new_list.append('BOOM')
        elif('7' in str(number)):
            new_list.append('BOOM')
        else:
            new_list.append(number)
    return new_list

print(seven_boom(17))
