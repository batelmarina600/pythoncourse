def squared_numbers(start, stop):
    """The function accepts two integers, start and stop (assume that: start <= stop). 
    The function returns a list containing all the squares of the numbers between start and stop 
    :param start: the start numbers for the loop
    :param stop: the end number
    :type start: int
    :type stop: int
    :return: list containing all the squares of the numbers between start and stop 
    :rtype: list
    """
    new_list = []
    while start <= stop:
       new_list.append(start ** 2)
       start += 1
    return new_list

print(squared_numbers(4, 8))
print(squared_numbers(-3, 3))
       