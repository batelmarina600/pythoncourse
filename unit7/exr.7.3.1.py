def show_hidden_word(secret_word, old_letters_guessed):
    """The string shows the letters from the old_letters_guessed list that are in the secret_word string 
    in their appropriate position, and the other letters in the string 
    (which the player has not yet guessed) as underlines.
    :param secret_word: represents the secret word that the player has to guess
    :param old_letters_guessed: a list contains the letters the player has guessed so far
    :type secret_word: string
    :type old_letters_guessed: list
    :return: the string that can be printed 
    :rtype: string
    """
    print_str = ""
    for char in secret_word:
        if(char in old_letters_guessed):
            print_str += char 
        else:
            print_str += "_"
        print_str += " "

    return print_str

secret_word = "mammals"
old_letters_guessed = ['s', 'p', 'j', 'i', 'm', 'k']
print(show_hidden_word(secret_word, old_letters_guessed))