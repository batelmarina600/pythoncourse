def sequence_del(my_str):
    """The function accepts a string and deletes the letters appearing in sequence. 
    The function returns a string in which only one character appears from each sequence
    of identical characters in the input string.
    :param my_str: the string that the function going to search on
    :type my_str: string
    :return: string that the sequences were deleted
    :rtype: list
    """
    new_str = ""
    for i in range(len(my_str)):
        if(i == 0):
            new_str +=my_str[0]
        elif (my_str[i]!=my_str[i-1]):
            new_str +=my_str[i]
    return new_str

print(sequence_del("ppyyyyythhhhhooonnnnn"))
print(sequence_del("SSSSsssshhhh"))
print(sequence_del("Heeyyy   yyouuuu!!!"))

       