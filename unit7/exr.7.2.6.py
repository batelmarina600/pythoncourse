def list_all_illegal(list_shopping):
    """finds all the illegal products - A product is invalid if it is less than 3 
    or contains non-alphanumeric characters
    :param my_str: the list that the function going to search on
    :type my_str: list
    :return: list og thr illegal products
    :rtype: list
    """
    new_list = []
    for item in list_shopping:
        if((len(item) < 3) or not (all(char.isalpha() for char in item))):
            new_list.append(item)
    return new_list

def deleting_duplicates(list_shopping):
    """deletes all the duplicats in the givien list
    :param my_str: the list that the function going to search on
    :type my_str: list
    :return: the list after the deletes
    :rtype: list
    """
    for item in list_shopping:
        if(list_shopping.count(item) > 1):
            list_shopping.remove(item)
    return list_shopping

def main():
    list_shopping = input("Enter list of products to shop: ")
    list_shopping = list_shopping.split(",")

    num = input("Enter action: ")

    while(num != "9"):
        
        if(num == "1"):
            print(list_shopping)
        if(num == "2"):
            print(len(list_shopping))
        if(num == "3"):
            product = input("Enter name of product: ")
            if(product in list_shopping):
                print("Yes, the product is in the list")
            else:
                print("No, the product is not in the list")

        if(num == "4"):
            product = input("Enter name of product: ")
            print("The product is " + str(list_shopping.count(product)) + " times")    

        if(num == "5"):
           product = input("Enter name of product: ")
           if(product in list_shopping):
                list_shopping.remove(product)
                print(list_shopping)
           else:
               print("The product is not in the list")

        if(num == "6"):
            product = input("Enter name of product: ")
            list_shopping.append(product)
            print(list_shopping)
        
        if(num == "7"):
            print(list_all_illegal(list_shopping))
            
        if(num == "8"):
            list_shopping = deleting_duplicates(list_shopping)
            print(list_shopping)

        num = input("Enter action: ")
        


if __name__ == "__main__":
    main()
