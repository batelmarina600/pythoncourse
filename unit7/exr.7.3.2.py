def check_win(secret_word, old_letters_guessed):
    """The function returns true if all the letters that make up the secret word are included 
    in the list of letters that the user guessed. Otherwise, the function returns false.
    :param secret_word: represents the secret word that the player has to guess
    :param old_letters_guessed: a list contains the letters the player has guessed so far
    :type secret_word: string
    :type old_letters_guessed: list
    :return: True if there is a win, False if not
    :rtype: bool
    """
    for char in secret_word:
        if(not (char in old_letters_guessed)):
            return False
    return True

secret_word = "friends"
old_letters_guessed = ['m', 'p', 'j', 'i', 's', 'k']
print(check_win(secret_word, old_letters_guessed))

secret_word = "yes"
old_letters_guessed = ['d', 'g', 'e', 'i', 's', 'k', 'y']
print(check_win(secret_word, old_letters_guessed))